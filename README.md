# `GPU_PBTE`

This is the starting code for `GPU_PBTE`.  

## What is `GPU_PBTE` ?

* `GPU_PBTE` stands for Graphics Processing Units accelerated Phonon Boltzmann Translation Equation solver.
* `GPU_PBTE` can take three-phonon scattering process, four-phonon scattering process and isotope scattering process into consideration and has been significantly accelerated by using GPUs. 
   
## Prerequisites

* You need to have a GPU card, `CUDA` toolkit and NVIDIA HPC SDK.
* It works for Linux (with GCC) operating systems. 
* Intel MKL(Math Kernel Library) is need for this program.

## Compile `GPU_PBTE`

* Go to the `PBTE` root directory and type `make help` to show help documentation and get started!

## Run `GPU_PBTE`

* Go to the `samples/Si` directory.
* Modify the `input/input.dat` file as needed.
* Type ` sh ./run/job1 ` to run job1 to mesh the Brillouin zone and obtain the eigenvector, frequency, group velocity and other physical quantities based on the diagonalized dynamical matrix.
* Type ` sh ./job2 ` to run job2 to calculate the phonon scattering processes.
* Type ` sh ./run/job3 ` to run job3 to solve the thermal conductivity.

## Manual

* The manual for GPU_PBTE will be available at a later date.

## Authors:

* Zhang Bo (Shanghai Jiao Tong University)
  * zhangbo.sjtu@qq.com
* Xiaokun Gu (Shanghai Jiao Tong University)
  * xiaokun.gu@sjtu.edu.cn

## Website:

* https://gitlab.com/guxiaokun11/GPU_PBTE
